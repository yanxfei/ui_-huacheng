function install(url, meth, token = "", vulue = {}) {
  let p = new Promise(function (resolve, reject) {
    uni.request({
      url: `http://localhost:8080/api/${url}`, //仅为示例，并非真实接口地址。
      method: meth,
      data: vulue,
      header: {
        token: token, //自定义请求头信息
      },
      success: (res) => {
        // console.log(res.data);
        resolve(res);
        // this.text = 'request success';
      },
    });
  });
  return p;
}

export { install };
