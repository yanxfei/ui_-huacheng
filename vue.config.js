module.exports = {
	// 配置路径别名
	configureWebpack: {
		devServer: {
			disableHostCheck: true
		}
	},
	
	devServer: {
	    proxy: {
	      "/api": {
	        target: "http://127.0.0.1:7001/",
	        // 允许跨域
	        changeOrigin: true,
	        ws: true,
	        pathRewrite: {
	          "^/api": ""
	        }
	      }
	    }
	  }


}
